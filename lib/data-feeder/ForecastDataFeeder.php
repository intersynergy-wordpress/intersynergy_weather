<?php 

  require_once(ISWP_BASE.'/lib/data-feeder/ISDataFeeder.php');

  class ForecastDataFeeder extends ISDataFeeder
  {
    const API_ENDPOINT = 'https://api.forecast.io/forecast/';
    private $api_key;

    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }

    private function request($latitude, $longitude, $time = null, $options = array())
    {
        $request_url = self::API_ENDPOINT
            . $this->api_key
            . '/'
            . $latitude
            . ','
            . $longitude
            . ((is_null($time)) ? '' : ','. $time);

        if (!empty($options)) {
            $request_url .= '?'. http_build_query($options);
        }
        
        $opts = array(
          'https'=>array(
            'method'=>"GET",
            'header'=>"Accept-Encoding: gzip\r\n"
          )
        );

        $context = stream_context_create($opts);

        $response = file_get_contents($request_url, false, $context);
        // $response->headers = $http_response_header;

        return json_decode($response, true);
    }

    public function getData($dataConfig)
    {
      $time = new DateTime();
      $data = $this->request(
        $dataConfig['latitude'], 
        $dataConfig['longitude'], 
        null,// $time->format('c'),
        array(
          'units' => 'si',
          // 'exclude' => 'daily',
          'extend' => 'daily'
        ));

      $placeData = array(
        'name' => $dataConfig['name'],
        'data' => $this->parseData($data)
      );

      return $placeData;
    }

    private function parseData($data)
    {
      $days = array();

      if (array_key_exists('daily', $data)) {
        foreach ($data['daily']['data'] as $dayIndex => $dayData) {
          if ($dayIndex < 7) {
            $days[] = $this->createDatDataArray($dayData);
          }
        }
      }

      return $days;
    }

    private function createDatDataArray($day)
    {
      $dayData = array();

      // Data
      $dayData['date'] = $day['time'];
      // Temperature
      $dayData['temp'] = round(($day['temperatureMax'] - $day['temperatureMin']) / 2, 1);
      // TemperatureMIN
      $dayData['temp_min'] = round($day['temperatureMin'], 1); 
      // TemperatureMAX
      $dayData['temp_max'] = round($day['temperatureMax'], 1);
      // Icon
      $dayData['icon'] = $day['icon'];
      // Type
      $dayData['type'] = $this->translate($day['summary']);
      // Humidity
      $dayData['humidity'] = $day['humidity'];
      // Wind
      $dayData['windSpeed'] = $day['windSpeed'];

      return $dayData;
    }

    private function translate($weatherSummary)
    {
      //@TODO Dodać tłumaczenia opisów.

      return $weatherSummary;
    }

  }