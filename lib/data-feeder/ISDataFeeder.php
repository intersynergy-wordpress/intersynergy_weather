<?php 

  abstract class ISDataFeeder
  {
    abstract public function getData($dataConfig);
  }