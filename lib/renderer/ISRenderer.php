<?php 

  abstract class ISRenderer
  {
    protected $data;

    public function setData($data)
    {
      $this->data = $data;
    }

    abstract function render();
  }