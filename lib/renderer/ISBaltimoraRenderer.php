<?php 
  require_once(ISWP_BASE.'/lib/renderer/ISRenderer.php');

  class ISBaltimoraRenderer extends ISRenderer
  {
    public function render()
    {
      $html = '<div class="hidden-xs is_weather_wrapper baltimora_view">'.
                '<div class="container">'.
                  '<div class="row">'.
                    '<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">'.
                      $this->renderContentShort($this->data['data'][0]).
                      '<div class="is_weather_content">';

          // Renderowanie dni
          foreach ($this->data['data'] as $dayIndex => $day) {
            $classActive = $dayIndex == 0 ? 'active' : '';

            $html .= $this->renderDay($day, $classActive);
          }
      

      $html .= '</div></div></div></div>';
      $html .= '<div class="hidden-xs is_weather_switcher text-center"><span class="glyphicon glyphicon-chevron-up"></span></div></div>';

      return $html;
    }

    private function renderContentShort($day)
    {
      $date = new DateTime();
      $date->setTimestamp($day['date']);
      $days = array('NIEDZIELA', 'PONIEDZIAŁEK', 'WTOREK', 'ŚRODA', 'CZWARTEK', 'PIĄTEK', 'SOBOTA');
      $months = array('STYCZEŃ', 'LUTY', 'MARZEC', 'KWIECIEŃ', 'MAJ', 'CZERWIEC', 
                      'LIPIEC', 'SIERPIEŃ', 'WRZESIEŃ', 'PAŹDZIERNIK', 'LISTOPAD', 'GRUDZIEŃ');

      return '<div class="is_weather_content_short show hidden-xs">'.
               '<div class="isw_day pull-left">'.$days[$date->format("w")].'</div>'.
               '<div class="isw_date pull-right">'.$date->format("j").' '.$months[$date->format("n") - 1].'</div>'.
               '<div class="clearfix"></div>'.
               '<div class="isw_day_content">'.
                 '<div class="isw_city">'.$this->data["name"].'</div>'.
                 '<div class="clearfix"></div>'.
                 '<div class="isw_temp pull-left"><span>'.$day["temp"].'</span><sup>0</sup></div>'.
                 '<div class="isw_icon pull-left"><img src="app/plugins/intersynergy_weather/assets/baltimora/img/'.$day["icon"].'.png" class="img-responsive"></div>'.
                 '<div class="isw-wind pull-left">'.
                   '<span class="isw-wind-icon"></span>'.
                   '<span class="isw-wind-value">'.$day["windSpeed"].' m/s</span>'.
                 '</div>'.
                 '<div class="clearfix"></div>'.
               '</div>'.
             '</div>';
    }

    private function renderDay($day, $activeClass) 
    {
      $html = '<div class="is_weather_day '.$activeClass.'">'.
                '<div class="is_weather_day_all">'.
                  $this->renderAllDayPart($day).
                '</div>'.
                '<div class="is_weather_day_short">'.
                  $this->renderShort($day).
                '</div>'.
              '</div>';

      return $html;
    }

    private function renderShort($day)
    {
      $date = new DateTime();
      $date->setTimestamp($day['date']);
      $days = array('ND.', 'PN.', 'WT.', 'ŚR.', 'CZ.', 'PT.', 'SB.');

      $html = '<div class="isw-day text-center">'.$days[$date->format("w")].'</div>'.
                '<div class="isw_icon text-center">'.
                  '<img src="app/plugins/intersynergy_weather/assets/baltimora/img/'.$day["icon"].'.png" class="img-responsive">'.
                '</div>'.
                '<div class="isw_temp text-center"><span>'.$day["temp"].'</span><sup>0</sup></div>'.
                '<div class="isw-wind text-center">'.
                  '<span class="isw-wind-icon"></span>'.
                  '<span class="isw-wind-value">'.$day["windSpeed"].'m/s</span>'.
                '</div>';

      return $html;
    }

    private function renderAllDayPart($day)
    {
      $date = new DateTime();
      $date->setTimestamp($day['date']);
      $days = array('NIEDZIELA', 'PONIEDZIAŁEK', 'WTOREK', 'ŚRODA', 'CZWARTEK', 'PIĄTEK', 'SOBOTA');
      $months = array('STYCZEŃ', 'LUTY', 'MARZEC', 'KWIECIEŃ', 'MAJ', 'CZERWIEC', 
                      'LIPIEC', 'SIERPIEŃ', 'WRZESIEŃ', 'PAŹDZIERNIK', 'LISTOPAD', 'GRUDZIEŃ');

      $html = '<div class="isw_header">'.
                '<div class="isw_day pull-left">'.$days[$date->format("w")].'</div>'.
                '<div class="isw_date pull-right">'.$date->format("j").' '.$months[$date->format("n") - 1].'</div>'.
                '<div class="clearfix"></div>'.
              '</div>'.
              '<div class="isw_day_content">'.
                '<div class="isw_day_left">'.
                  '<div class="isw_city">'.$this->data["name"].'</div>'.
                  '<div class="isw_temp"><span>'.$day["temp"].'</span><sup>0</sup></div>'.
                  '<div class="isw_temp_min">TEMP MIN: <span>'.$day["temp_min"].'</span><sup>0</sup></div>'.
                  '<div class="isw_temp_max">TEMP MAX: <span>'.$day["temp_max"].'</span><sup>0</sup></div>'.
                '</div>'.
                '<div class="isw_day_right">'.
                  '<div class="isw_icon"><img src="app/plugins/intersynergy_weather/assets/baltimora/img/'.$day["icon"].'.png" class="img-responsive"></div>'.
                  '<div class="isw_type">'.$day["type"].'</div>'.
                  '<div class="isw-humidity">'.
                    '<span class="isw-humidity-icon"></span>'.
                    '<span class="isw-humidity-value">'.$day["humidity"].'</span>%'.
                  '</div>'.
                  '<div class="isw-wind">'.
                    '<span class="isw-wind-icon"></span>'.
                    '<span class="isw-wind-value">'.$day["windSpeed"].'</span>m/s'.
                  '</div>'.
                '</div>'.
              '</div>';


      return $html;
    }
  }