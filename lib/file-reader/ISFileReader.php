<?php 

  abstract class ISFileReader
  {
    protected $rootDir;
    protected $fileExt;


    public function __construct($rootDir)
    {
      $this->rootDir = $rootDir;
    }

    abstract public function load($file);
    abstract public function save($file, $data);

    public function hasFile($file)
    {
      return file_exists($this->rootDir.'/'.$file.$this->fileExt);
    }

    protected function getContent($file)
    {
      if ($this->hasFile($file)) {
        return file_get_contents($this->rootDir.'/'.$file.$this->fileExt);
      }

      return false;
    }

    protected function setContent($file, $content)
    {
      file_put_contents($this->rootDir.'/'.$file.$this->fileExt, $content);
    }

  }