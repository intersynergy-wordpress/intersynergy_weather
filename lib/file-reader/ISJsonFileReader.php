<?php

  require_once(ISWP_BASE.'/lib/file-reader/ISFileReader.php');

  class ISJsonFileReader extends ISFileReader
  {
    protected $fileExt = '.json';

    public function load($file)
    {
      return json_decode($this->getContent($file), true);
    }

    public function save($file, $data)
    {
      $this->setContent($file, json_encode($data));
    }
  }