<?php

  abstract class ISDataManager
  {
    protected $dataFeeder;


    public function __construct(ISDataFeeder $dataFeeder)
    {
      $this->dataFeeder = $dataFeeder;
    }

    public function getData($dataSlug)
    {
      if ($this->hasData($dataSlug)) {
        return $this->retriveData($dataSlug);
      }

      return false;
    }

    public function update($dataSlug, $dataConfig)
    {
      try {
        $data = $this->dataFeeder->getData($dataConfig);
        $this->saveData($dataSlug, $data);
      } catch (Exception $e) {
        echo $e->getMessage();
        die();
      }
    }

    abstract protected function hasData($dataSlug);
    abstract protected function retriveData($dataSlug);
    abstract protected function saveData($dataSlug, $data);
  }