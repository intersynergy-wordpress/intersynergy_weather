<?php 
  
  require_once(ISWP_BASE.'/lib/data-manager/ISDataManager.php');
  require_once(ISWP_BASE.'/lib/data-feeder/ISDataFeeder.php');
  require_once(ISWP_BASE.'/lib/file-reader/ISFileReader.php');

  class ISFileDataManager extends ISDataManager
  {
    protected $fileReader;


    public function __construct(ISDataFeeder $dataFeeder, ISFileReader $fileReader)
    {
      parent::__construct($dataFeeder);

      $this->fileReader = $fileReader;
    }

    protected function hasData($dataSlug)
    {
      return $this->fileReader->hasFile(
        $this->makeFileName($dataSlug));
    } 

    protected function retriveData($dataSlug)
    {
      try {
        return $this->fileReader->load(
          $this->makeFileName($dataSlug));   
      } catch (Exception $e) {
        return array();
      }
    }

    protected function saveData($dataSlug, $data)
    {
      $this->fileReader->save(
          $this->makeFileName($dataSlug),
          $data);   
    }

    protected function makeFileName($dataSlug)
    {
      return md5($dataSlug);
    }
  }
