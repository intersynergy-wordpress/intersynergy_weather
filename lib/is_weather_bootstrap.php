<?php 
  require_once(ISWP_BASE.'/lib/core/InterSynergyWeatherCore.php');
  require_once(ISWP_BASE.'/lib/file-reader/ISJsonFileReader.php');
  require_once(ISWP_BASE.'/lib/data-feeder/ForecastDataFeeder.php');
  require_once(ISWP_BASE.'/lib/data-manager/ISFileDataManager.php');
  require_once(ISWP_BASE.'/lib/core/ISPlaceConfig.php');
  require_once(ISWP_BASE.'/lib/renderer/ISBaltimoraRenderer.php');

  // cofnig
  $configReader = new ISJsonFileReader(ISWP_BASE.'/config');
  $placeConfig  = new ISPlaceConfig($configReader, 'place_config');

  // dataManager
  $dataFeeder   = new ForecastDataFeeder('e9b25afd09988c9fdd27e11b566317d3');
  $dmFileReader = new ISJsonFileReader(ISWP_BASE.'/data-dir');
  $dataManager  = new ISFileDataManager($dataFeeder, $dmFileReader);
  
  // renderer
  $renderer = new ISBaltimoraRenderer();

  // core
 return new InterSynergyWeatherCore($placeConfig, $dataManager, $renderer);