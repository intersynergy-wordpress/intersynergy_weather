<?php

  require_once(ISWP_BASE.'/lib/data-manager/ISDataManager.php');
  require_once(ISWP_BASE.'/lib/core/ISPlaceConfig.php');
  require_once(ISWP_BASE.'/lib/renderer/ISRenderer.php');

  class InterSynergyWeatherCore
  {
  protected $placeConfig;
  protected $dataManager;
  protected $renderer;

  public function __construct(ISPlaceConfig $placeConfig, ISDataManager $dataManager, ISRenderer $renderer)
  {
    $this->placeConfig = $placeConfig;
    $this->dataManager = $dataManager;
    $this->renderer    = $renderer;
  }

  public function update()
  {
    foreach ($this->placeConfig->getPlaces() as $place) {
      $this->dataManager->update(
        $place, $this->placeConfig->getPlace($place)
      );
    }
  }

  public function getData($placeSlug)
  {
    return  $this->dataManager->getData($placeSlug);
  }

  public function render($placeSlug)
  {
    $this->renderer->setData($this->getData($placeSlug));

    return $this->renderer->render();
  }
 }