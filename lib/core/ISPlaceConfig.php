<?php 

  class ISPlaceConfig 
  {
    protected $fileReader;
    protected $fileConfig;
    protected $config;

    public function __construct(ISFileReader $fileReader, $fileConfig)
    {
      $this->fileReader = $fileReader;
      $this->fileConfig = $fileConfig;

      $this->config = $this->fileReader->load($this->fileConfig);
    }

    public function getPlaces()
    {
      return array_keys($this->config);
    }

    public function getPlace($placeSlug)
    {
      if (array_key_exists($placeSlug, $this->config)) {
        return $this->config[$placeSlug];
      }

      return null;
    }
  }