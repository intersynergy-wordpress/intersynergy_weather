<div class="is_weather_day_all">
  <div class="isw_header">
    <div class="isw_day">PONIEDZIAŁEK</div>
    <div class="isw_date">23 KWIECIEŃ</div>
  </div>
  <div class="isw_day_content">
    <div class="isw_day_left">
      <div class="isw_city">SZCZECIN</div>
      <div class="isw_temp"><span>26</span><sup>0</sup></div>
      <div class="isw_temp_min"><span>16</span><sup>0</sup></div>
      <div class="isw_temp_max"><span>29</span><sup>0</sup></div>
    </div>
    <div class="isw_day_right">
      <div class="isw_icon"><img src="" class="img-responsive"></div>
      <div class="isw_type">CZĘŚCIOWE ZACHMURZENIE</div>
      <div class="isw-humidity">
        <span class="isw-humidity-icon"></span>
        <span class="isw-humidity-value">34</span>%
      </div>
      <div class="isw-wind">
        <span class="isw-wind-icon"></span>
        <span class="isw-wind-value">91</span>km/h
      </div>
    </div>
  </div>
</div>
<div class="is_weather_day_short">
  <div class="isw-day">PN.</div>
  <div class="isw_icon">
    <img src="" class="img-responsive">
  </div>
  <div class="isw_temp"><span>56</span><sup>0</sup></div>
  <div class="isw-wind">
    <span class="isw-wind-icon"></span>
    <span class="isw-wind-value">91</span>km/h
  </div>
</div>