jQuery(document).ready(function($) {

  if (typeof window.ISW != 'object') {
    window.ISW = {};
  }

  window.ISW = {
    rootEl: $('.is_weather_wrapper.baltimora_view'),

    init: function() {
      var _this = this;

      _this.rootEl.delegate('.is_weather_day', 'click', function() {
        var clickedElem = $(this);
        var activeElem = _this.rootEl.find('.is_weather_day.active');

        if (clickedElem != activeElem) {
          _this.selectDay(clickedElem, activeElem);
        }
      });

      _this.rootEl.delegate('.is_weather_switcher', 'click', function() {
        var button = $(this);

        if (button.find('span').hasClass('glyphicon-chevron-up')) {
          _this.rootEl.find('.is_weather_content_short').removeClass('show');
          _this.rootEl.find('.is_weather_content').delay(300).addClass('show');

          button.find('span').removeClass('glyphicon-chevron-up')
                             .addClass('glyphicon-chevron-down');
        } else {
          _this.rootEl.find('.is_weather_content_short').addClass('show');
          _this.rootEl.find('.is_weather_content').delay(300).removeClass('show');

          button.find('span').addClass('glyphicon-chevron-up')
                             .removeClass('glyphicon-chevron-down');
        }
      });
    },

    selectDay: function(clickedEl, activeEl, callback) {
      var _this = this;

      activeEl.removeClass('active');
      clickedEl.addClass('active');
    }
  }


  window.ISW.init();
});