<?php 
  /*
  Plugin Name: InterSynergy Weather
  Description: Informacje o pogodzie
  Version: 1.0
  Author: Karol Ćwikła
  */

  define('ISWP_BASE', dirname(__FILE__));


  // $isWeatherCore->update();
  // var_dump($isWeatherCore->getData('gdansk'));

  class intersynergy_weather extends WP_Widget
  {
    function intersynergy_weather()
    {
        $options = array(
            'description' => 'Informacje o pogodzie' 
        );

        $this->WP_Widget('intersynergy_weather', 'InterSynergy Weather', $options);
    }

    function form($instance)
    {
      $instance = wp_parse_args( (array)$instance, 
        array(
          'title' => '',
          'view'  => 'baltimora_view'
        )
      );

      $title = esc_attr($instance['title']);
      $view  = $instance['view'];
?>
      <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">Tytuł:</label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('view'); ?>">Widok:</label><br />

        <select id="<?php echo $this->get_field_id('view'); ?>" 
                name="<?php echo $this->get_field_name('view'); ?>">
          <option <?php if ($view == 'baltimora'): ?>selected <?php endif; ?> value="baltimora">Baltimora</option>
          <option <?php if ($view == 'mila'): ?>selected <?php endif; ?> value="mila">Mila</option>
        </select>
      </p>
<?php
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        $instance['title'] = esc_attr($new_instance['title']);
        $instance['view']  = $new_instance['view'];

        return $instance;
    }

    function widget($args, $instance)
    {
      $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
      $view  = $instance['view'];

      // include assets
      wp_register_script('is_weather_js', plugins_url('/assets/'.$view.'/js/main.js', __FILE__), array('jquery'));
      wp_enqueue_script('is_weather_js');

      wp_register_style('is_weather_css', plugins_url('/assets/'.$view.'/css/style.css', __FILE__));
      wp_enqueue_style('is_weather_css');


      if (!empty($title)) {
          echo $before_title . $title . $after_title;
      };

      $isWeatherCore = include(ISWP_BASE.'/lib/is_weather_bootstrap.php');
      
      echo $isWeatherCore->render('gdansk');
    }
  }

  add_action('widgets_init', create_function('', 'return register_widget("intersynergy_weather");'));